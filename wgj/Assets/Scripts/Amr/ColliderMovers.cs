﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColliderMovers : MonoBehaviour
{
    public TestCameraMove cameraMove;
    public float cameraSpeed = 5;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("Enter");
        if (collision.CompareTag("Player"))
        {
            cameraMove.lerpSpeed = cameraSpeed;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            cameraMove.lerpSpeed = 0;
        }
    }
}
