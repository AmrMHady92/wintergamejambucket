﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine;

public class FillBar : MonoBehaviour
{
    public Image fillingImage;
    public UnityEvent onFill; 
    public void StartFilling(float duration = 5)
    {
        LeanTween.cancel(this.gameObject);
        LeanTween.value(this.gameObject,0, 1, duration).setOnUpdate((float v) => 
        {
            fillingImage.fillAmount = v;
        }).setOnComplete(()=> 
        {
            onFill?.Invoke();
        });
    }

    public void StopFilling()
    {
        LeanTween.cancel(this.gameObject);
        this.gameObject.SetActive(false);
    }
}
