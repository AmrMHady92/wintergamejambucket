﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollower : MonoBehaviour
{
    public float worldLimitsLeft = -5;
    public float worldLimitsRight = -5;
    public float maxDistance = 2;
    public float height = 2;
    public GameObject playerObject;
    private void Update()
    {
        if(this.transform.position.x >= worldLimitsLeft && this.transform.position.x <= worldLimitsRight)
        {
            if (Mathf.Abs(this.transform.position.x - playerObject.transform.position.x) > maxDistance)
            {
                if (this.transform.position.x > playerObject.transform.position.x)
                {
                    //this.transform.position = playerObject.transform.position + new Vector3(maxDistance, 0, 0);
                    this.transform.position = new Vector3(playerObject.transform.position.x + maxDistance, this.transform.position.y, this.transform.position.z); ;

                }
                else
                {
                    this.transform.position = new Vector3(playerObject.transform.position.x - maxDistance, this.transform.position.y, this.transform.position.z); ;
                }
            }

            //this.transform.position = new Vector3(this.transform.position.x, playerObject.transform.position.y + height, this.transform.position.z);
        }
        if (this.transform.position.x < worldLimitsLeft) this.transform.position = new Vector3(worldLimitsLeft, this.transform.position.y, this.transform.position.z);
        if (this.transform.position.x > worldLimitsRight) this.transform.position = new Vector3(worldLimitsRight, this.transform.position.y, this.transform.position.z);
    }
}
