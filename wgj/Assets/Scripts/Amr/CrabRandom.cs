﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrabRandom : MonoBehaviour
{
    public void RandomAnimationSetter()
    {

        this.GetComponent<Animator>().SetInteger("RandomAnimation", Random.Range(0, 3));
    }

}
