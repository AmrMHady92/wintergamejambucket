﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Interactable : MonoBehaviour
{
    public GameObject interactImage;
    public bool interacted = false;
    public abstract void Interact();
    public virtual void SetInteractableImage(bool onOff = true)
    {
        if (!interacted || !onOff)
        {
            if (interactImage != null) interactImage.gameObject.SetActive(onOff);
        }
    }

    private bool shaking = false;
    public virtual void ShakeButton()
    {
        if (shaking) return;
        shaking = true;
        Vector3 startPos = interactImage.transform.position;
        LeanTween.move(interactImage, startPos + new Vector3(0.2f, 0, 0), 0.1f).setEaseOutElastic().setOnComplete(()=> 
        {
            LeanTween.move(interactImage, startPos - new Vector3(0.2f, 0, 0), 0.1f).setEaseOutElastic().setOnComplete(() =>
            {
                LeanTween.move(interactImage, startPos, 0.1f).setEaseOutElastic().setOnComplete(() =>
                {
                    shaking = false;
                });
            });
        });
    }

    private bool clicking = false;

    public virtual void ButtonClicked()
    {
        if (clicking) return;
        clicking = true;
        Vector3 startScale = interactImage.transform.localScale;
        LeanTween.scale(interactImage, startScale - new Vector3(0.1f, 0.1f, 0.1f), 0.1f).setEaseOutElastic().setOnComplete(() =>
        {
            LeanTween.scale(interactImage, startScale + new Vector3(0.1f, 0.1f, 0.1f), 0.1f).setEaseOutElastic().setOnComplete(() =>
            {
                LeanTween.scale(interactImage, startScale, 0.1f).setEaseOutElastic().setOnComplete(() =>
                {
                    clicking = false;
                    SetInteractableImage(false);
                });
            });
        });
    }




}
