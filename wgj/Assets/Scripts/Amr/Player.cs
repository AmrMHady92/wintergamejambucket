﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{

    private PlayerAnimationController playerAnimator;
    private CharacterController controller;
    Interactable currentInteractable;

    private static Player instance;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            if(CurrentInteractable != null)
            {
                CurrentInteractable.Interact();
            }
        }
    }

    private void Awake()
    {
        if (instance == null) instance = this;
        controller = this.GetComponent<CharacterController>();
        playerAnimator = GameObject.FindObjectOfType<PlayerAnimationController>();

    }
    public Interactable CurrentInteractable
    {
        get
        {
            return currentInteractable;
        }

        set
        {
            if (currentInteractable != null) currentInteractable.SetInteractableImage(false);
            currentInteractable = value;
            if (currentInteractable != null) currentInteractable.SetInteractableImage(true);

        }
    }

    public static Player Instance
    {
        get
        {
            return instance;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Interactable interactable = collision.GetComponent<Interactable>();
        if(interactable != null)
        {
            Debug.Log("Hit "+ interactable.gameObject.name);
            CurrentInteractable = interactable;
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        Interactable interactable = collision.GetComponent<Interactable>();
        if (interactable != null)
        {
            CurrentInteractable = null;
        }
    }

    public void SetMovementOnOff(bool onOff = false)
    {
        controller.active = onOff;
    }

    public void PlayerActivate()
    {
        StopAllCoroutines();
        StartCoroutine(BlockMovement());
        playerAnimator.DoAction();
    }

    private IEnumerator BlockMovement()
    {
        controller.active = false;
        yield return new WaitForSeconds(1.75f);
        controller.active = true;
    }
}
