﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{
    Rigidbody2D rb;
    PlayerAnimationController animator;
    public GameObject playerSprite;
    public float speed = 5f;

    public bool active = true;
    public bool lookingRight = true;

    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        animator = GameObject.FindObjectOfType<PlayerAnimationController>();
    }

    void Update()
    {
        if (!active)
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
            animator.Speed = 0;

            return;
        }
        if (Input.GetKey(KeyCode.A))
        {
            rb.velocity = new Vector2(-speed, rb.velocity.y);
            if (lookingRight)
            {
                lookingRight = false;
                playerSprite.transform.localScale = new Vector3(Mathf.Abs(playerSprite.transform.localScale.x) * -1, playerSprite.transform.localScale.y, playerSprite.transform.localScale.z);
            }
        }
        else
        if (Input.GetKey(KeyCode.D))
        {
            rb.velocity = new Vector2(speed , rb.velocity.y);
            if (!lookingRight)
            {
                lookingRight = true;
                playerSprite.transform.localScale = new Vector3(Mathf.Abs(playerSprite.transform.localScale.x), playerSprite.transform.localScale.y, playerSprite.transform.localScale.z);
            }

        }
        else
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
        }

     

        animator.Speed = Mathf.Abs(rb.velocity.x);
    }
}
