﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSlower : Obstacle
{
    public OldGuyMoving oldGuy;
    public override void OnGuyEnter()
    {
        oldGuy.speed *= 0.5f;
    }

    public override void OnGuyExit()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
