﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class ObstacleStop : Obstacle
{
    public OldGuyMoving oldGuy;
    public bool activated = false;

    public UnityEvent onTriggered;
    public override void OnGuyEnter()
    {
        oldGuy.speed = 0;
        activated = true;
        onTriggered?.Invoke();
    }

    public override void OnGuyExit()
    {
    }


}
