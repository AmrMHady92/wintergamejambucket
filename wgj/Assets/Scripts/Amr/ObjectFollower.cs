﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectFollower : MonoBehaviour
{
    public Transform followedTransform;
    public Vector3 spacing;
    public float lerpSpeed = 10;
    void Update()
    {
        this.transform.position = Vector3.Lerp(this.transform.position, followedTransform.transform.position + spacing, lerpSpeed * Time.deltaTime);
    }
}
