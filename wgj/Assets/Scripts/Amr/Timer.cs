using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using TMPro;
using System;

    public class Timer : MonoBehaviour
    {
        [SerializeField] private UnityEvent onTimeEnd;
        public bool startTimerOnGameStart = false;
        public int gameTime;
        private bool ticking = false;

        TextMeshProUGUI text;
        DateTime startTime;
        int gameSeconds = 0;
        int mins = 0;
        int seconds = 0;

        public static Timer Instance;

        public UnityEvent OnTimeEnd
        {
            get
            {
                if (onTimeEnd == null) onTimeEnd = new UnityEvent();
                return onTimeEnd;
            }

            set
            {
                onTimeEnd = value;
            }
        }

        public bool Ticking
        {
            get
            {
                return ticking;
            }

            set
            {
                ticking = value;
            }
        }

        private void Awake()
        {
            if (Instance == null) Instance = this;
        }
        void Start()
        {
            text = this.GetComponent<TextMeshProUGUI>();
            if(text == null) text = this.GetComponentInChildren<TextMeshProUGUI>();

            if (startTimerOnGameStart) StartTimer(gameTime);
        }

        public void StartTimer(int totalSeconds = 1, bool useConfig = false)
        {
            if (useConfig) totalSeconds = gameTime;
            if (totalSeconds <= 0) totalSeconds = 1;
            StopAllCoroutines();
            ticking = true;
            startTime = DateTime.Now;
            gameSeconds = totalSeconds;
            CalculateTime();
            StartCoroutine(TimeTick());
            
        }

        public void QuickStart()
        {
            StartTimer(useConfig: true);
        }


        private IEnumerator TimeTick()
        {
            while (Ticking)
            {
                gameSeconds--;
                CalculateTime();

                if (gameSeconds == 0)
                    TimeEnded();
                yield return new WaitForSeconds(1);

            }
        }

        private void CalculateTime()
        {
            mins = gameSeconds / 60;
            seconds = gameSeconds - mins * 60;
            UpdateText(seconds, mins);
        }

        private void TimeEnded()
        {
            StopAllCoroutines();
            CalculateTime();
            OnTimeEnd?.Invoke();
            Debug.Log(OnTimeEnd);
            ticking = false;
            /*if (OnTimeEnd.GetPersistentEventCount() > 0) */
            Debug.Log("GameEnded");
        }

        private void UpdateText(int seconds, int mins)
        {
            string minsString = mins.ToString();
            string secsString = seconds.ToString();

            if (mins < 10) minsString = "0" + mins.ToString();
            if (seconds < 10) secsString = "0" + seconds.ToString();

            text.text = (minsString + ":" + secsString);
        }
    }
