﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

[System.Serializable]
public class InventoryItem
{
    public Sprite icon;
    public string itemName = "Item";

} 
public class PlayerInventory : MonoBehaviour
{

    public List<InventoryItem> inventoryList;
    public GameObject imageIconContainer;
    private Pooler pooler;
    public Transform canvasContainer;
    public float start = -75;
    public float spacing = -120;
    private List<GameObject> icons;

    void Start()
    {
        inventoryList = new List<InventoryItem>();
        pooler = canvasContainer.gameObject.AddComponent<Pooler>();
        pooler.PooledObject = imageIconContainer;
    }

    public bool CheckInventoryItem(InventoryItem item)
    {
        if(item != null)
        {
            return inventoryList.Contains(item);
        }
        else
        {
            return false;
        }
    }

    public bool CheckInventoryItem(string itemName = "")
    {
        if (itemName != "")
        {
           InventoryItem item = inventoryList.Find(x => (x.itemName == itemName));

            if (item != null)
            {
                return true;
            }
            else return false;
        }
        else
        {
            return false;
        }
    }

    public void AddItem(InventoryItem item)
    {
        if (!CheckInventoryItem(item))
        {
            inventoryList.Add(item);
        }
        UpdateUI();
    }



    public void RemoveItem(InventoryItem item)
    {
        if (CheckInventoryItem(item))
        {
            inventoryList.Remove(item);
        }
        UpdateUI();
    }

    public void RemoveItem(string itemName)
    {

        if (CheckInventoryItem(itemName))
        {
            InventoryItem item = inventoryList.Find(x => (x.itemName == itemName));
            if(item != null)
            {
                Debug.Log("Found To Remove");
                inventoryList.Remove(item);
            }
        }
        UpdateUI();
    }

    private void UpdateUI()
    {
        if (icons != null)
        {
            for (int i = 0; i < icons.Count; i++)
            {
                icons[i].SetActive(false);
            }
        }
        icons = new List<GameObject>();
        GameObject icon;
        Image iconImage;

        for (int i = 0; i < inventoryList.Count; i++)
        {
            icon = pooler.Get(true);
            icons.Add(icon);
            iconImage = icon.GetComponent<Image>();
            iconImage.sprite = inventoryList[i].icon;
            icon.transform.parent = canvasContainer;
            icon.transform.localScale = Vector3.one;
            icon.transform.localPosition = new Vector3(start + i * spacing, 0, 0);
        }
    }
}
