﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableShadeyMerchant : Interactable
{
    enum MerchantState
    {
        ClosedCoat,
        OpenedCoatWBroom,
        OpenedCoatWoutBroom
    }
    private SpriteRenderer merchantSprite;
    public InventoryItem broomItem;
    public Sprite closedCoat;
    public Sprite opennedCoatWithBroom;
    public Sprite opennedCoatWithOutBroom;

    public bool gotBroom = false;

    private MerchantState state;

    private MerchantState State
    {
        get
        {
            return state;
        }

        set
        {
            state = value;
            switch (state)
            {
                case MerchantState.ClosedCoat:
                    merchantSprite.sprite = closedCoat;
                    break;
                case MerchantState.OpenedCoatWBroom:
                    merchantSprite.sprite = opennedCoatWithBroom;
                    break;
                case MerchantState.OpenedCoatWoutBroom:
                    merchantSprite.sprite = opennedCoatWithOutBroom;
                    break;
                default:
                    break;
            }
        }
    }

    public override void Interact()
    {
        if (interacted) return;
        interacted = true;
        ButtonClicked();
        switch (State)
        {
            case MerchantState.ClosedCoat:
                if (!gotBroom)
                {
                    StartCoroutine(ChangeStateInTime(0.2f, MerchantState.OpenedCoatWBroom, false));
                }
                else
                {
                    State = MerchantState.OpenedCoatWoutBroom;
                    StartCoroutine(ChangeStateInTime(2f, MerchantState.ClosedCoat, false));
                }
                break;
            case MerchantState.OpenedCoatWBroom:

                
                PlayerInventory playerInventory = GameObject.FindObjectOfType<PlayerInventory>();
                playerInventory.AddItem(broomItem);
                Player.Instance.PlayerActivate();
                gotBroom = true;
                State = MerchantState.OpenedCoatWoutBroom;
                StartCoroutine(ChangeStateInTime(2f, MerchantState.ClosedCoat, false));
                break;
        }

       
        
    }

    private void Start()
    {
        merchantSprite = this.GetComponent<SpriteRenderer>();
        State = MerchantState.ClosedCoat;

    }

    private IEnumerator ChangeStateInTime(float time, MerchantState state, bool interact)
    {
        yield return new WaitForSeconds(time);
        State = state;
        interacted = interact;
    }
}
