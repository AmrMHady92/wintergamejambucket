﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableTrafficLights : Interactable
{
    public ObstacleStop obstacleObject;
    public SpriteRenderer spriteRenderer;
    public Sprite redLight;
    public Sprite orangeLight;
    public Sprite greenLight;
    public override void Interact()
    {
        if (interacted) return;
        //SetInteractableImage(false);
        ButtonClicked();
        StartCoroutine(TrafficLightSequence());
        Player.Instance.PlayerActivate();

        interacted = true;
    }
    private void Start()
    {
        spriteRenderer = this.GetComponent<SpriteRenderer>();
    }

    private IEnumerator TrafficLightSequence()
    {
        yield return new WaitForSeconds(3);
        spriteRenderer.sprite = orangeLight;
        yield return new WaitForSeconds(3);
        obstacleObject.gameObject.SetActive(false);
        if(obstacleObject.activated)
        {
            OldGuyMoving oldmover = GameObject.FindObjectOfType<OldGuyMoving>();
            oldmover.speed = oldmover.mainSpeed;
        }
        spriteRenderer.sprite = greenLight;
        yield return new WaitForSeconds(6);
        spriteRenderer.sprite = redLight;
        obstacleObject.gameObject.SetActive(true);
        obstacleObject.activated = false;
        interacted = false;
    }
}
