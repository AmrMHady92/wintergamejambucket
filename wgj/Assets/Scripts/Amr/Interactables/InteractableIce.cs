﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableIce : Interactable
{
    public ObstacleStop obstacleObject;
    public SpriteRenderer iceRenderer;
    public Sprite sandedIce;
    public override void Interact()
    {
        if (interacted) return;
        PlayerInventory inventory = GameObject.FindObjectOfType<PlayerInventory>();
        if(inventory != null)
        {
            Debug.Log("Found Inv");
            if (inventory.CheckInventoryItem("Sand"))
            {
                Debug.Log("Found Item");
                Player.Instance.PlayerActivate();
                inventory.RemoveItem("Sand");
                interacted = true;
                obstacleObject.gameObject.SetActive(false);
                if(obstacleObject.activated)
                {
                    OldGuyMoving oldmover = GameObject.FindObjectOfType<OldGuyMoving>();
                    oldmover.speed = oldmover.mainSpeed;
                }
                ButtonClicked();
                //SetInteractableImage(false);
                iceRenderer.sprite = sandedIce;

            }
            else
            {
                ShakeButton();
            }
        }
    }



}
