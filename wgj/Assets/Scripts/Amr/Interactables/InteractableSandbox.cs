﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableSandbox : Interactable
{

    public InventoryItem item;

    public override void Interact()
    {
        if (interacted) return;
        interacted = true;
        ButtonClicked();
        //SetInteractableImage(false);
        PlayerInventory playerInventory = GameObject.FindObjectOfType<PlayerInventory>();
        //InventoryItem itemnew = new InventoryItem();
        //itemnew.icon = item.icon;
        playerInventory.AddItem(item);
        Player.Instance.PlayerActivate();
    }

}
