﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableCrabs : Interactable
{
    public ObstacleStop obstacleObject;
    public List<GameObject> crabs;

    public override void Interact()
    {
        if (interacted) return;
        PlayerInventory inventory = GameObject.FindObjectOfType<PlayerInventory>();

        if (inventory.CheckInventoryItem("Broom"))
        {
            Debug.Log("Found Item");
            Player.Instance.PlayerActivate();
            inventory.RemoveItem("Broom");
            interacted = true;
            obstacleObject.gameObject.SetActive(false);
            if (obstacleObject.activated)
            {
                OldGuyMoving oldmover = GameObject.FindObjectOfType<OldGuyMoving>();
                oldmover.speed = oldmover.mainSpeed;
            }

            for (int i = 0; i < crabs.Count; i++)
            {
                crabs[i].transform.LeanScale(Vector3.zero, 1);
            }

            ButtonClicked();
            //SetInteractableImage(false);

        }
        else
        {
            ShakeButton();
        }
    }
}
