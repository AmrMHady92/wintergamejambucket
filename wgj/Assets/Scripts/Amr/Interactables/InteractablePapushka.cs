﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractablePapushka : Interactable
{

    public ObstacleStop obstacleStop;
    public GameObject bubbleImage;
    public AudioClip plaplaClip;
    public AudioClip letsgoClip;
    public OldGuyMoving mover;

    private AudioSource asource;

    public override void Interact()
    {
        if (interacted) return;
        interacted = true;
        asource.loop = false;
        asource.clip = letsgoClip;
        asource.Play();
        ButtonClicked();
        //SetInteractableImage(false);
        bubbleImage.SetActive(false);
        Player.Instance.PlayerActivate();
        mover.speed = mover.mainSpeed;

    }

    public void Activate()
    {
        asource.clip = plaplaClip;
        asource.Play();
        bubbleImage.SetActive(true);
        interacted = false;
        obstacleStop.gameObject.SetActive(false);
    }
    void Start()
    {
        asource = gameObject.AddComponent<AudioSource>();
        asource.playOnAwake = false;
        asource.loop = true;
        interacted = true;
    }


}
