﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour
{

    public Timer timer;
    OldGuyMoving mover;

    public GameObject gameOverWindow;
    public GameObject gameWonWindow;


    private void Start()
    {
        mover = GameObject.FindObjectOfType<OldGuyMoving>();
        mover.speed = 0;
        Player.Instance.SetMovementOnOff(false);
    }


    public void StartGame()
    {
        timer.gameObject.SetActive(true);
        timer.StartTimer(timer.gameTime);
        mover.speed = 1;
        Player.Instance.SetMovementOnOff(true);
    }
    public void TimeEnded()
    {
        StopMovement();

        gameOverWindow.SetActive(true);
        gameOverWindow.transform.localScale = Vector3.zero;
        //LeanTween.rotateZ(gameOverWindow, 360, 2);
        LeanTween.value(gameOverWindow, 0,360, 0.5f).setOnUpdate((float v)=> { gameOverWindow.transform.eulerAngles = new Vector3(0, 0, v); });
        LeanTween.scale(gameOverWindow, Vector3.one, 0.5f);
    }

    private void StopMovement()
    {
        mover.speed = 0;
        Player.Instance.SetMovementOnOff(false);
    }

    public void Reached()
    {
        StopMovement();

        timer.enabled = false;
        timer.gameObject.SetActive(false);

        gameWonWindow.SetActive(true);
        gameWonWindow.transform.localScale = Vector3.zero;
        //LeanTween.rotateZ(gameWonWindow, 360, 2);
        LeanTween.value(gameWonWindow, 0, 360, 0.5f).setOnUpdate((float v) => { gameWonWindow.transform.eulerAngles = new Vector3(0, 0, v); });
        LeanTween.scale(gameWonWindow, Vector3.one, 0.5f);
    }

    public void RestartPressed()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void ScaleDownGameObejct(GameObject gO)
    {
        LeanTween.scale(gO, Vector3.zero, 0.5f).setOnComplete(()=> { gO.SetActive(false); });
    }
}
