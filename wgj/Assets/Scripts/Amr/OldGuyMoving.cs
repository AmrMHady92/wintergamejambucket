﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OldGuyMoving : MonoBehaviour
{
    private Rigidbody2D rb;
    private Old_Lady_Animator ladyAnimator;
    public float speed = 1;
    public bool isWalking = false;
    public float mainSpeed = 1;
    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        ladyAnimator = this.GetComponentInChildren<Old_Lady_Animator>();
        if (Mathf.Abs(rb.velocity.x) > 0) isWalking = true;
        //mainSpeed = speed;
    }
    void Update()
    {
        if (isWalking)
            rb.velocity = new Vector2(1 * speed, rb.velocity.y);
        else rb.velocity = new Vector2(0, rb.velocity.y);

        ladyAnimator.Speed = Mathf.Abs(rb.velocity.x);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Obstacle obst = collision.GetComponent<Obstacle>();
        if (obst != null)
        {
            obst.OnGuyEnter();
        }
    }
}
