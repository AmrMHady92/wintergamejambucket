﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Old_Lady_Animator : MonoBehaviour
{

    private Animator animator;
    private float speed;

    public float Speed
    {
        get
        {
            return speed;
        }

        set
        {
            speed = value;
            animator.SetFloat("Speed", speed);
        }
    }

    void Start()
    {
        animator = GetComponent<Animator>();
    }




}

